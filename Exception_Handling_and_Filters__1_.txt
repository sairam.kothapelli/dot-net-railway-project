======================================================================================================================
			   *Exception Handling and Filter iin ASP.Net Core MVC*
======================================================================================================================

Exception Handling :

- Exception handling in ASP.NET Core MVC is essential for gracefully handling errors and exceptions that can occur during the execution of your web application. 
- Effective exception handling ensures that users receive meaningful error messages and that unexpected issues are logged and addressed.

a) Configure Exception Handling Middleware: 
- ASP.NET Core provides built-in middleware for handling exceptions. 
- we can configure it in the Startup.cs file within the Configure method. 
note : Make sure it is placed early in the middleware pipeline to catch exceptions.


public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
{
    if (env.IsDevelopment())
    {
        app.UseDeveloperExceptionPage();
    }
    else
    {
        // Configure production error handling here.
        app.UseExceptionHandler("/Home/Error");
        app.UseStatusCodePagesWithRedirects("/Home/Error/{0}");
    }

    // Other middleware configuration.
}

-- exceptions are redirected to a specified error page.

b) Custom Error Pages: 
- Create custom error pages to provide a user-friendly experience. 
- In this the UseExceptionHandler middleware redirects exceptions to the /Home/Error route. 
- we can create this route and corresponding action method to display custom error views.

public IActionResult Error()
{
    return View();
}


c) Logging:
- Use a logging framework like Serilog, NLog, or the built-in ASP.NET Core logger to log exceptions and errors.
- Logging helps you diagnose issues and monitor application health.


try
{
    // Code that may throw exceptions.
}
catch (Exception ex)
{
    _logger.LogError(ex, "An error occurred.");
}

d) Exception Filters:
- we can use exception filters to handle exceptions at the action method level. 
- These filters can be applied to specific controllers or action methods.


[TypeFilter(typeof(CustomExceptionFilter))]
public IActionResult SomeAction()
{
    // Code that may throw exceptions.
}
- Create a custom exception filter by implementing IExceptionFilter interface and handle exceptions within the OnException method.

e) Global Exception Handling:
- Implement global exception handling by creating a custom middleware.
- This middleware can catch unhandled exceptions and provide a consistent response format.

public class GlobalExceptionMiddleware
{
    private readonly RequestDelegate _next;
    private readonly ILogger<GlobalExceptionMiddleware> _logger;

    public GlobalExceptionMiddleware(RequestDelegate next, ILogger<GlobalExceptionMiddleware> logger)
    {
        _next = next;
        _logger = logger;
    }

    public async Task Invoke(HttpContext context)
    {
        try
        {
            await _next(context);
        }
        catch (Exception ex)
        {
            _logger.LogError(ex, "An error occurred.");
            context.Response.StatusCode = StatusCodes.Status500InternalServerError;
            await context.Response.WriteAsync("An unexpected error occurred. Please try again later.");
        }
    }
}

f) Use Status Codes: 
- Return appropriate HTTP status codes in your action methods to indicate different error scenarios.
 For example, use NotFound (404) for resource not found, BadRequest (400) for validation errors, and InternalServerError (500) for unexpected server errors.

By following these practices, you can effectively handle exceptions in your ASP.NET Core MVC application, ensuring a better user experience and easier debugging and maintenance.


___________________________________________________________________________________________________________________________________________________________


Filters :

- allow you to add pre-processing and post-processing logic to the execution of action methods. 
- They are a powerful mechanism for adding cross-cutting concerns like authorization, validation, logging, and caching to the application in a modular and reusable way.

1) Authorization Filters: These filters are used to control access to action methods based on user authentication and authorization. The commonly used authorization filter is the AuthorizeFilter, which restricts access to authenticated users or users with specific roles.


[Authorize(Roles = "Admin")]
public IActionResult AdminAction()
{
    // Action logic for admin users.
}

2) Action Filters: 
- Action filters run before and after an action method is executed. 
- used to perform tasks like logging, input validation, or modifying the action's result. 
- we can implement your custom action filters by implementing the IActionFilter interface.


public class LogActionFilter : IActionFilter
{
    public void OnActionExecuting(ActionExecutingContext context)
    {
        // Executed before the action method.
        // Perform pre-processing tasks.
    }

    public void OnActionExecuted(ActionExecutedContext context)
    {
        // Executed after the action method.
        // Perform post-processing tasks.
    }
}

3)Result Filters: 
- Result filters run before and after the execution of the result of an action method (e.g., view rendering). 
- used to modify the response, log, or perform other operations related to the result. 
- we can create custom result filters by implementing the IResultFilter interface.

public class LogResultFilter : IResultFilter
{
    public void OnResultExecuting(ResultExecutingContext context)
    {
        // Executed before the result is executed.
        // Perform pre-processing tasks.
    }

    public void OnResultExecuted(ResultExecutedContext context)
    {
        // Executed after the result is executed.
        // Perform post-processing tasks.
    }
}


4) Exception Filters: 
- used to handle exceptions that occur during the execution of an action method. 
- allow you to perform custom exception handling and logging. 
- Create custom exception filters by implementing the IExceptionFilter interface.

public class CustomExceptionFilter : IExceptionFilter
{
    public void OnException(ExceptionContext context)
    {
        // Handle and log exceptions.
        context.Result = new ViewResult
        {
            ViewName = "Error"
        };
        context.ExceptionHandled = true;
    }
}



5) Resource Filters: 
- run before and after model binding and action method execution. 
- useful for tasks like caching and setting up resources needed for action execution.

6) Type Filters: 
- Type filters are a way to apply filters to all action methods within a controller or all controllers within an application by specifying them at the controller or application level.
____________________________________________________________________________________________________________________________________________________________
~ To use filters in your ASP.NET Core MVC application, you can apply them at various levels, including at the action method, controller, or global application level, depending on your requirements. 
~ Filters provide a flexible way to inject behavior into your application's request processing pipeline, making your application more modular, maintainable, and robust.

============================================================================================================================================================
 