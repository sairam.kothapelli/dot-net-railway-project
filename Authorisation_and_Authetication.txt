===========================================================================================================================================================
     			Athorisation and Authetication in Asp.net MVC Core
===========================================================================================================================================================

-- Authentication is the process of verifying the identity of a user. 
-- Authorization is the process of determining whether an authenticated user has permission to access a specific resource.

In ASP.NET Core MVC, authentication and authorization are implemented using middleware. 
Middleware is a software component that sits between the web server and the application. 
It can be used to perform a variety of tasks, such as logging, caching, and authentication.

ASP.NET Core MVC provides a number of built-in authentication middleware components, such as Cookie Authentication and Identity authentication. 
We can also use third-party authentication middleware components.

Once a user has been authenticated, We can use the Authorize attribute to restrict access to controllers, actions, and views.

-----------------------------------------------------------------------------------------------------------------------------------------------------------

--> Authentication:

- Authentication is the process of verifying the identity of a user, ensuring that they are who they claim to be. 
- involves validating the credentials of a user, typically through mechanisms like username and password, tokens, or other authentication providers (e.g., social logins). 
- Once a user is authenticated, the application assigns an identity to them, which is used to track and recognize the user's interactions with the application.

ASP.NET Core MVC provides a flexible authentication system that supports various authentication methods, including:

a) Cookie-based Authentication: This is commonly used for traditional username and password-based authentication. It involves storing an authentication cookie on the user's device.

b) JWT (JSON Web Tokens) Authentication: JWTs are commonly used for token-based authentication, often in combination with APIs.

c) External Authentication Providers: we can integrate with external authentication providers like Google, Facebook, or Microsoft to allow users to log in with their existing accounts on these platforms.

d) IdentityServer: For more advanced scenarios, IdentityServer is an open-source framework for implementing OAuth 2.0 and OpenID Connect authentication and authorization. 

Ex:
---------------------------------
[Authorize]
public IActionResult Index()
{
    // ...
}
---------------------------------
___________________________________________________________________________________________________________________________________________________________

--> Authorization:

- Authorization is the process of determining what actions and resources a user is allowed to access or perform within an application after they have been authenticated. 
- It involves setting access control rules and policies to restrict or grant access to certain features or data based on a user's identity and permissions.

ASP.NET Core MVC provides a flexible and attribute-based authorization system. 
You can use attributes like [Authorize] on controllers or actions to specify who can access them. 

Common authorization scenarios include:

a) Role-Based Authorization: we can assign users to roles (e.g., Admin, User) and restrict access to certain features based on roles.

b) Policy-Based Authorization: we can define custom authorization policies that specify who can access specific resources or actions based on complex criteria beyond just roles.

c) Claims-Based Authorization: we can make authorization decisions based on claims present in a user's identity. Claims are key-value pairs associated with a user's identity and can be used to represent roles, permissions, or other user-specific information.

d) Resource-Based Authorization: we can authorize users based on the specific resource they are trying to access. For example, ensuring that a user can only modify their own data.

Ex:
----------------------------------
[Authorize(Roles = "Admin")]
public IActionResult Index()
{
    // ...
}
----------------------------------

===========================================================================================================================================================
